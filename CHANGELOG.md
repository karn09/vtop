# Changelog

## 0.2.8

- Improve Makefile

## 0.2.7 - 2014-06-12

- Add Becca theme. http://www.zeldman.com/2014/06/10/the-color-purple/

## 0.2.6 - 2014-06-11

- Fix major perf problem that some users were having relating to 
  drawing the header for update notifications.

## 0.2.5

- Delete old data points to improve performance

## 0.2.4, 0.2.3, 0.2.2 - 2014-06-11

- Fixes to the upgrade script
- Added --version and -V command line arguments for version

## 0.2.0 - 2014-06-11

- Add new theme 'dark'
- Add auto-update mechanism

## 0.1.7 - 2014-06-11

- Add a fix for user submitted ps outputs - #5 #6 #7
- Fix issue with 'NaN' appearing for some users

## 0.1.4 - 2014-06-09

- Slice the row strings smaller in table view

## 0.1.0 - 2014-06-08

- First release